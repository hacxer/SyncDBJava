package com.hzaccp.syncdbjava.window;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Toolkit;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.hzaccp.syncdbjava.entity.ConConfig;
import com.hzaccp.syncdbjava.entity.Table;
import com.hzaccp.syncdbjava.helper.ConnectionHelper;
import com.hzaccp.syncdbjava.helper.ExcelHelper;
import com.hzaccp.syncdbjava.helper.MssqlDBHelper;
import com.hzaccp.syncdbjava.window.biz.EntityBuilderBiz;
import com.hzaccp.syncdbjava.window.biz.SyncDbBiz;

public class MainWindow extends java.awt.Frame {
	private static final long serialVersionUID = -4259339680151285634L;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd hh:mm:ss");//时间格式

	FileDialog fileDialog = null;//文件选择器
	Map<String, Table> excelTables = new HashMap<String, Table>();
	private String[] columnStrs = new String[] { "", "表名", "中文名", "包名", "" };//列名
	private ConConfig con;//连接信息

	public MainWindow() {
		initComponents();
		initWindow();
	}

	/**
	 * 初始化窗口
	 * */
	private void initWindow() {
		this.setResizable(false);//不可修改大小
		this.setTitle("同步");
		this.setSize(640, 480);

		//初始化文件选择器
		fileDialog = new FileDialog(this);
		fileDialog.setMultipleMode(true);//多选

		//初始化窗口位置
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = this.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		this.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);

		//初始化表格
		initTabTable(null);

		print("根目录:" + getProjectPaht());
	}

	/**
	 * 测试数据库连接
	 * */
	private void btnTestConActionPerformed(java.awt.event.ActionEvent evt) {
		String url = txtURL.getText();
		String user = txtUser.getText();
		String pwd = txtPassword.getText();
		String dbName = txtDB.getText();

		con = new ConConfig(url, user, pwd);
		if (ConnectionHelper.testConnection(con)) {
			print("连接成功.");
		} else {
			print("连接失败.");
		}

		con.setDbName(dbName);
		if (!ConnectionHelper.testConnection(con)) {
			print("[" + dbName + "]库不存在或无权限.");
		}
	}

	/**
	 * 点击同步
	 * */
	private void btnBuildingActionPerformed(java.awt.event.ActionEvent evt) {
		if (!isSelect()) {
			return;//没有选中的行
		}

		//有选中的行

		print("开始同步...");
		print("开始测试连接...");

		String url = txtURL.getText();
		String user = txtUser.getText();
		String pwd = txtPassword.getText();
		String dbName = txtDB.getText();

		con = new ConConfig(url, user, pwd);

		//测试连接
		boolean isConOk = ConnectionHelper.testConnection(con);
		if (!isConOk) {
			print("连接失败");
			print("同步失败");
			return;
		}
		print("连接成功");
		con.setDbName(dbName);

		//测试数据库是否存在或有权限
		boolean isDBOk = ConnectionHelper.testConnection(con);
		if (!isDBOk) {
			print("正在初始化数据库...");
			MssqlDBHelper.initDB(url, user, pwd, dbName);
			print("数据库初始化完毕..");
		}

		//开始同步表	
		String filePath = getProjectPaht() + "createTable.sql";

		Map<String, Table> excelTemps = new HashMap<String, Table>();
		for (int i = 0; i < tabTables.getRowCount(); i++) {
			Boolean isSelect = (Boolean) tabTables.getModel().getValueAt(i, 0);
			if (isSelect) {
				Table t = (Table) tabTables.getModel().getValueAt(i, 4);
				excelTemps.put(t.getTableName(), t);
			}
		}

		SyncDbBiz.builderTable(this, con, excelTemps);//同步库
		SyncDbBiz.builderSql(this, excelTemps, filePath);//生成sql脚本

		print("同步完成\r\n");
	}

	/**
	 * 点击生成实体
	 * */
	private void btnBuildEntityActionPerformed(java.awt.event.ActionEvent evt) {
		Map<String, Table> excelTemps = new HashMap<String, Table>();
		for (int i = 0; i < tabTables.getRowCount(); i++) {
			Boolean isSelect = (Boolean) tabTables.getModel().getValueAt(i, 0);
			if (isSelect) {
				Table t = (Table) tabTables.getModel().getValueAt(i, 4);
				excelTemps.put(t.getTableName(), t);
			}
		}
		EntityBuilderBiz.builder(this, excelTemps);//生成实体
		print("生成完成\r\n");
	}

	/**
	 * 选择文件
	 * */
	private void btnSelectFileActionPerformed(java.awt.event.ActionEvent evt) {
		fileDialog.setVisible(true);//显示文件选择器
		File[] fs = fileDialog.getFiles();
		if (fs.length > 0) {
			excelTables.clear();//清空所有
			for (int i = 0; i < fs.length; i++) {
				try {
					Map<String, Table> ts = ExcelHelper.getAllTables(fs[i], true);
					excelTables.putAll(ts);
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
			initTabTable(null);//将excelTables中的数据，添加到tabTabes控件中
		}
	}

	/**
	 * 得到项目根目录
	 * */
	public static String getProjectPaht() {
		return System.getProperty("user.dir");
	}

	/**
	 * 将excelTables中的数据，添加到tabTabes控件中
	 * @param str 过滤字符
	 * */
	private void initTabTable(String str) {
		tabTables.removeAll();
		ArrayList<Object[]> arrTemp = new ArrayList<Object[]>();
		for (String key : excelTables.keySet()) {
			if (str != null) {//需要过滤
				if (key.indexOf(str) < 0) {
					continue;
				}
			}
			Table ttemp = excelTables.get(key);
			arrTemp.add(new Object[] { true, ttemp.getTableName(), ttemp.getTableName_ch(), ttemp.getPackName(), ttemp });
		}

		//构建数据数组
		Object[][] tarr = new Object[arrTemp.size()][5];
		for (int i = 0; i < tarr.length; i++) {
			for (int j = 0; j < 5; j++) {
				tarr[i][j] = arrTemp.get(i)[j];
			}
		}
		tabTables.setModel(new DefaultTableModel(tarr, columnStrs));

		//第一列为选择列
		JCheckBox c = new JCheckBox();
		tabTables.getColumnModel().getColumn(0).setCellRenderer(new TableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JCheckBox se = new JCheckBox();
				if ("true".equals(value.toString())) {
					se.setSelected(true);
				}
				return se;
			}
		});
		tabTables.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(c));
		setCW(0, 20);
		setCW(1, 100);
		setCW(2, 100);
		setCW(3, 200);
		setCW(4, 10);
	}

	/**
	 * 设置列宽
	 * */
	private void setCW(int index, int width) {
		TableColumn c = tabTables.getColumnModel().getColumn(index);
		if (c != null) {
			c.setPreferredWidth(width);
			c.setMinWidth(width);
		}
	}

	/**
	 * 判断tabTables是否有选中的行
	 * */
	private boolean isSelect() {
		for (int i = 0; i < tabTables.getRowCount(); i++) {
			Boolean isSelect = (Boolean) tabTables.getModel().getValueAt(i, 0);
			if (isSelect) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 将信息输出到控制台
	 * */
	public void print(String str) {
		txtConsole.append(sdf.format(new Date()) + "-->" + str + "\r\n");
	}

	//==================================以下是编辑器生成代码============================

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		btnSelectFile = new java.awt.Button();
		scrTables = new javax.swing.JScrollPane();
		tabTables = new javax.swing.JTable();
		btnBuilding = new java.awt.Button();
		labDBtitile = new java.awt.Label();
		labURL = new java.awt.Label();
		txtURL = new java.awt.TextField();
		labUser = new java.awt.Label();
		txtUser = new java.awt.TextField();
		labPassword = new java.awt.Label();
		txtPassword = new java.awt.TextField();
		btnTestCon = new java.awt.Button();
		txtConsole = new java.awt.TextArea();
		labDB = new java.awt.Label();
		txtDB = new java.awt.TextField();
		btnBuildEntity = new java.awt.Button();

		setBackground(java.awt.SystemColor.control);
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				exitForm(evt);
			}
		});
		setLayout(null);

		btnSelectFile.setLabel("\u9009\u62e9\u6587\u4ef6");
		btnSelectFile.setName("");
		btnSelectFile.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnSelectFileActionPerformed(evt);
			}
		});
		add(btnSelectFile);
		btnSelectFile.setBounds(10, 30, 70, 26);

		tabTables.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {}, new String[] { "", "表名", "中文名", "包名", "" }));
		scrTables.setViewportView(tabTables);

		add(scrTables);
		scrTables.setBounds(10, 60, 420, 180);

		btnBuilding.setLabel("\u540c\u6b65");
		btnBuilding.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnBuildingActionPerformed(evt);
			}
		});
		add(btnBuilding);
		btnBuilding.setBounds(90, 30, 40, 26);

		labDBtitile.setText("sqlServer2005\u6570\u636e\u5e93\u914d\u7f6e");
		add(labDBtitile);
		labDBtitile.setBounds(450, 60, 180, 22);

		labURL.setText("\u8def\u5f84:");
		add(labURL);
		labURL.setBounds(450, 90, 31, 22);

		txtURL.setText("jdbc:sqlserver://127.0.0.1");
		add(txtURL);
		txtURL.setBounds(500, 90, 130, 22);

		labUser.setText("\u8d26\u53f7:");
		add(labUser);
		labUser.setBounds(450, 150, 31, 22);

		txtUser.setText("sa");
		add(txtUser);
		txtUser.setBounds(500, 150, 130, 22);

		labPassword.setText("\u5bc6\u7801:");
		add(labPassword);
		labPassword.setBounds(450, 180, 31, 22);

		txtPassword.setText("123456789");
		add(txtPassword);
		txtPassword.setBounds(500, 180, 130, 22);

		btnTestCon.setLabel("\u6d4b\u8bd5");
		btnTestCon.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnTestConActionPerformed(evt);
			}
		});
		add(btnTestCon);
		btnTestCon.setBounds(570, 210, 57, 26);
		add(txtConsole);
		txtConsole.setBounds(10, 250, 620, 220);

		labDB.setText("\u5e93:");
		add(labDB);
		labDB.setBounds(450, 120, 19, 22);

		txtDB.setText("test");
		add(txtDB);
		txtDB.setBounds(500, 120, 130, 22);

		btnBuildEntity.setActionCommand("\u751f\u6210\u5b9e\u4f53");
		btnBuildEntity.setLabel("\u751f\u6210\u5b9e\u4f53");
		btnBuildEntity.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnBuildEntityActionPerformed(evt);
			}
		});
		add(btnBuildEntity);
		btnBuildEntity.setBounds(140, 30, 70, 26);

		pack();
	}// </editor-fold>
		//GEN-END:initComponents

	/** Exit the Application */
	private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
		System.exit(0);
	}//GEN-LAST:event_exitForm

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainWindow().setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private java.awt.Button btnBuildEntity;
	private java.awt.Button btnBuilding;
	private java.awt.Button btnSelectFile;
	private java.awt.Button btnTestCon;
	private java.awt.Label labDB;
	private java.awt.Label labDBtitile;
	private java.awt.Label labPassword;
	private java.awt.Label labURL;
	private java.awt.Label labUser;
	private javax.swing.JScrollPane scrTables;
	private javax.swing.JTable tabTables;
	private java.awt.TextArea txtConsole;
	private java.awt.TextField txtDB;
	private java.awt.TextField txtPassword;
	private java.awt.TextField txtURL;
	private java.awt.TextField txtUser;
	// End of variables declaration//GEN-END:variables

}